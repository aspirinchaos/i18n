import { ReactiveVar } from 'meteor/reactive-var';
import LocalizedStrings from 'localized-strings';
import defaultErrors from './default-errors.json';

/**
 * Реактивная переменная для хранения локали
 */
const currentLanguage = new ReactiveVar('ru');

/**
 * Класс для синглтона работы с локализациями
 */
class I18N {
  /**
   * Хралище переводов
   * @type LocalizedStrings
   */
  strings;

  /**
   * Хралище ключей ошибок
   * Ошибки хранятся отдельно, потому что они делаются на en-US
   * @type LocalizedStrings
   */
  errors;

  constructor() {
    Object.defineProperties(this, {
      initial: {
        value: 'ru',
      },
    });
    this.errors = new LocalizedStrings(defaultErrors);
  }

  get currentLanguage() {
    return currentLanguage.get();
  }

  setLanguage(lang) {
    import(`/i18n/${lang}.json`).then((file) => {
      if (this.strings) {
        this.strings.setContent(
          Object.assign({}, this.strings.getContent(), {
            [lang]: file.default,
          }),
        );
      } else {
        this.strings = new LocalizedStrings({
          [lang]: file.default,
        });
      }
      return import(`/i18n/${lang}-errors.json`);
    }).then((file) => {
      this.errors.setContent(
        // мерджим со старым контентом
        Object.assign({}, this.errors.getContent(), {
          [lang]: {
            // а тут мерджим дефолтные ошибки с новыми
            ...this.errors.getContent()[lang],
            ...file.default,
          },
        }),
      );
      currentLanguage.set(lang);
    }).catch((e) => {
      console.warn('Не забудьте добавить динамический импорт для сборщика!');
      console.log(e);
    });
  }

  translate(key, ...strings) {
    if (this.strings && this.strings[key]) {
      if (strings && strings.length) {
        return this.errors.formatString(this.strings[key], ...strings);
      }
      return this.strings[key];
    }
    if (this.initial === currentLanguage.get()) {
      if (strings && strings.length) {
        return this.errors.formatString(key, ...strings);
      }
      return key;
    }
    if (strings && strings.length) {
      return this.errors.formatString(this.strings[key], ...strings);
    }
    return this.strings[key];
  }

  translateError(key) {
    if (!this.errors[key]) {
      console.warn('Добавьте ключ к ошибкам');
    }
    return this.errors[key] || '';
  }
}

/**
 * Синглтон локализации
 * @type {I18N}
 */
const i18n = new I18N();

/**
 * Функция для переводов строки
 * @param {string} key - ключ строки
 * @param {Array<string>} strings - слова для тимплейт строки
 * @private
 */
const _t = (key, ...strings) => i18n.translate(key, ...strings);

/**
 * Функция для переводов ошибок
 * @param key {string}
 * @private
 */
const _e = key => i18n.translateError(key);

export { _t, _e, i18n };
