Package.describe({
  name: 'i18n',
  version: '0.1.0',
  summary: 'Пакет для работы с локализацией',
  git: 'https://bitbucket.org/aspirinchaos/i18n',
});

Npm.depends({
  'localized-strings': '0.2.0',
});

Package.onUse((api) => {
  api.versionsFrom('1.7');
  api.use(['ecmascript', 'reactive-var']);
  api.mainModule('i18n.js');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('i18n');
  api.mainModule('i18n-tests.js');
});
